import React from "react";
import logo from "./images/profile_pic.jpeg";
import videoIcon from "./images/icons/video_icon.png";
import chatIcon from "./images/icons/chat_icon.png";
import catalogIcon from "./images/icons/shopping_icon.png";
import quillIcon from "./images/icons/quill_icon.png";
import notificationIcon from "./images/icons/notification_icon.png"
import Catalog from './catalog/Catalog'



function App() {
  return (
    <div class="flex flex-col">
      <div class = "flex sticky justify-between top-0 bg-white p-6 w-full">
        <p class = "text-2xl">Coze</p>
        <img src={notificationIcon} class="h-8 pl-6 pr-6" alt="logo" />
      </div>
      <div class="flex-col">
        <div class="flex justify-center">
          <img src={logo} class="h-48 w-48 rounded-full" alt="logo" />
        </div>
        <h1 class="font-sans-bold text-center text-3xl">Kaylee</h1>
        <p class="text-l font-sans-bold text-center p-2">Hello lamp post, watcha knowin?</p>
      </div>
      <div class="flex-col bg-gray-400">
        <div class="flex justify-between">
          <p class="text-xl font-sans-bold text-center text-black pt-2 pl-6">
            My people
          </p>
          <p class="text-xl font-sans-bold text-center text-black pt-2 pr-6">
            Invite new
          </p>
        </div>
        <div class="flex flex-wrap p-6">
          <div class="p-6">picture</div>
          <div class="p-6">picture</div>
          <div class="p-6">picture</div>
          <div class="p-6">picture</div>
          <div class="p-6">picture</div>
          <div class="p-6">picture</div>
          <div class="p-6">picture</div>
          <div class="p-6">picture</div>
          <div class="p-6">picture</div>
          <div class="p-6">picture</div>
        </div>
      </div>
      <div class="flex border border-gray-500 h-56">
        <div class="flex-col">
          <p class="text-xl font-sans-bold text-black pt-2 pl-6">
            My free time
          </p>
          <p class="pt-2 pl-6">Schedule image goes here</p>
        </div>
      </div>
      <div class="flex bg-gray-400 h-56">
        <div class="flex-col ">
          <p class="text-xl font-sans-bold text-black pt-2 pl-6">My goals</p>
          <p class="pt-2 pl-6">Call mom more</p>
        </div>
      </div>
      <div class="flex border border-gray-500 h-56">
        <p class="text-xl font-sans-bold text-center text-black pt-2 pl-6">
          My photo albums
        </p>
      </div>
      <div class="flex bg-gray-400 h-56">
        <p class="text-xl font-sans-bold text-center text-black pt-2 pl-6">
          My stories
        </p>
      </div>
      <Catalog />
        <div class="flex p-6 justify-between border-gray-500">
          <img src={videoIcon} class="h-8 pl-6 pr-6" alt="logo" />
          <img src={chatIcon} class="h-8 pl-6 pr-6" alt="logo" />
          <img src={quillIcon} class="h-8 pl-6 pr-6" alt="logo" />
          <img src={catalogIcon} class="h-8 pl-6 pr-6" alt="logo" />
        </div>
    </div>
  );
}

export default App;
